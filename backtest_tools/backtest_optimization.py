from multiprocessing import Pool

METRICS_INPUT = ['Annual Return', 'Maximum Drawdown', 'Sharpe Ratio',
                 'Sortino Ratio', 'Calmar Ratio', 'Win Rate', 'Frequency']


class ParametersComparison:

    def __init__(self, df, **parameters_dict):
        self._df = df
        self._param_dict = parameters_dict
        self._params = [self._param_dict[i]['name'] for i in parameters_dict.keys()]
        self._params_range = [self._param_dict[i]['range'] for i in parameters_dict.keys()]

    def heatmap_plot(self):
        pass

    pass
