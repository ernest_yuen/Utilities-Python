# Utilities Python
Python 3.7.x+ Utilities Library.

## Getting Started

### Installation
#### Ubuntu 18.04 (Debian-based Linux)
```shell script
python3.7 -m pip install git+ssh://git@gitlab.com/<path>
```
```shell script
python3.7 -m pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>
```
#### Windows 10
```shell script
pip install git+ssh://git@gitlab.com/<path>
```
```shell script
pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>
```
